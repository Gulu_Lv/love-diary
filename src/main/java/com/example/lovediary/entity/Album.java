package com.example.lovediary.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "album")
public class Album implements Serializable {
    private String cid;
    private String albumId;
    private String title;
    private String cover;
    private int total;
    private Date createTime;
}
