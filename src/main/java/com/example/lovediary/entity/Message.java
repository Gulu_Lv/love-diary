package com.example.lovediary.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "message")
public class Message implements Serializable {
    private String mid;
    private String fromId;
    private String toId;
    // temp 1:查看 0未查看
    private String temp;
    // type 1:申请绑定消息  
    private String type;
    private String text;
}
