package com.example.lovediary.service;

import com.example.lovediary.entity.Album;
import com.example.lovediary.entity.Photo;

import java.util.List;
import java.util.Map;

public interface AlbumService {
    Map<Object,Object> setAlbumData(String albumId, int pageNumber);
    List<Album> getAlbum(String albumId);
    List<Photo> setEditPhotoText(String location);
    void editPhotoText(String location,String text);
    void deletePhoto(String location);
    void deletePhotoAlbum(String albumId,int total);
    void deleteAlbum(String albumId);
    void addPhoto(Photo photo);
    void addPhotoAlbum(String albumId,int total);
    void addAlbum(Album album);
}
