package com.example.lovediary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.lovediary.entity.Album;
import com.example.lovediary.entity.Photo;
import com.example.lovediary.mapper.AlbumMapper;
import com.example.lovediary.mapper.PhotoMapper;
import com.example.lovediary.service.AlbumService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class AlbumServiceImpl implements AlbumService {
    final AlbumMapper albumMapper;
    final PhotoMapper photoMapper;

    public AlbumServiceImpl(AlbumMapper albumMapper, PhotoMapper photoMapper) {
        this.albumMapper = albumMapper;
        this.photoMapper = photoMapper;
    }

    @Override
    public  Map<Object,Object> setAlbumData(String albumId, int pageNumber) {
        QueryWrapper<Photo> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("album_id",albumId).orderByAsc("create_time");
        Page<Photo> page = new Page<>(pageNumber,8);
        IPage<Photo> iPage= photoMapper.selectPage(page,wrapperUser);
        int totalPageNumber =(int) iPage.getPages();
        Map<Object,Object> map = new HashMap<>();
        map.put("totalPageNumber",""+totalPageNumber);
        List<Photo> photos = iPage.getRecords();
        map.put("photos",photos);
        return map;
    }

    @Override
    public List<Album> getAlbum(String albumId) {
        QueryWrapper<Album> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("album_id",albumId);
        return albumMapper.selectList(wrapperUser);
    }

    @Override
    public List<Photo> setEditPhotoText(String location) {
        QueryWrapper<Photo> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("location",location);
        return photoMapper.selectList(wrapperUser);
    }

    @Override
    public void editPhotoText(String location,String text) {
        UpdateWrapper<Photo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("location",location);
        Photo photo = new Photo();
        photo.setText(text);
        photoMapper.update(photo,updateWrapper);
    }

    @Override
    public void deletePhoto(String location) {
        QueryWrapper<Photo> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("location",location);
        photoMapper.delete(wrapperUser);
    }

    @Override
    public void deletePhotoAlbum(String albumId,int total) {
        UpdateWrapper<Album> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("album_id",albumId);
        Album album = new Album();
        album.setTotal(total-1);
        albumMapper.update(album,updateWrapper);
    }

    @Override
    public void deleteAlbum(String albumId) {
        QueryWrapper<Album> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("album_id",albumId);
        albumMapper.delete(wrapperUser);
    }

    @Override
    public void addPhoto(Photo photo) {
        photoMapper.insert(photo);
    }

    @Override
    public void addPhotoAlbum(String albumId, int total) {
        UpdateWrapper<Album> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("album_id",albumId);
        Album album = new Album();
        album.setTotal(total+1);
        albumMapper.update(album,updateWrapper);
    }

    @Override
    public void addAlbum(Album album) {
        albumMapper.insert(album);
    }
}
