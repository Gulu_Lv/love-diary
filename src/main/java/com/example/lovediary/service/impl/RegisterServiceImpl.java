package com.example.lovediary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.lovediary.entity.User;
import com.example.lovediary.mapper.UserMapper;
import com.example.lovediary.service.RegisterService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional(rollbackFor = Exception.class)
public class RegisterServiceImpl implements RegisterService {
    final UserMapper userMapper;

    public RegisterServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }
    // 注册判断 是否已经注册
    @Override
    public List<User> selectIsUser(String email) {
        QueryWrapper<User> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("email", email);
        return userMapper.selectList(wrapperUser);
    }
    // 存入数据库
    @Override
    public void insertUser(User user) {
        userMapper.insert(user);
    }
}
