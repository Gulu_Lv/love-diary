package com.example.lovediary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.lovediary.entity.Plan;
import com.example.lovediary.mapper.PlanMapper;
import com.example.lovediary.service.PlanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class PlanServiceImpl implements PlanService {
    final PlanMapper planMapper;

    public PlanServiceImpl(PlanMapper planMapper) {
        this.planMapper = planMapper;
    }

    @Override
    public List<Plan> selectPlan(String cid) {
        QueryWrapper<Plan> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("cid", cid);
        return planMapper.selectList(wrapperUser);
    }

    @Override
    public List<Plan> getPlan(String pid) {
        QueryWrapper<Plan> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("pid", pid);
        return planMapper.selectList(wrapperUser);
    }

    @Override
    public void editPlan(String pid,String status,String plan) {
        UpdateWrapper<Plan> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("pid",pid);
        Plan plan0 = new Plan();
        plan0.setPlan(plan);
        plan0.setStatus(status);
        planMapper.update(plan0,updateWrapper);
    }

    @Override
    public void deletePlan(String pid) {
        QueryWrapper<Plan> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("pid", pid);
        planMapper.delete(wrapperUser);
    }

    @Override
    public void addPlan(Plan plan) {
        planMapper.insert(plan);
    }
}
