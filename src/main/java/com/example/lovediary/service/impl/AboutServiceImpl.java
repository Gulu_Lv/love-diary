package com.example.lovediary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.lovediary.entity.Message;
import com.example.lovediary.entity.User;
import com.example.lovediary.mapper.MessageMapper;
import com.example.lovediary.mapper.UserMapper;
import com.example.lovediary.service.AboutService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class AboutServiceImpl implements AboutService {
    final UserMapper userMapper;
    final MessageMapper messageMapper;

    public AboutServiceImpl(UserMapper userMapper, MessageMapper messageMapper) {
        this.userMapper = userMapper;
        this.messageMapper = messageMapper;
    }
    // 发出绑定邀请时 被邀请人是否存在
    @Override
    public List<User> selectIsUser(String email) {
        QueryWrapper<User> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("email", email);
        return userMapper.selectList(wrapperUser);
    }
    // 邀请消息存入数据库
    @Override
    public void insertInvitation(Message message) {
        messageMapper.insert(message);
    }
}
