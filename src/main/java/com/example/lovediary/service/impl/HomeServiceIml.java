package com.example.lovediary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.lovediary.entity.*;
import com.example.lovediary.mapper.*;
import com.example.lovediary.service.HomeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class HomeServiceIml implements HomeService {
    final UserMapper userMapper;
    final ConnectionMapper connectionMapper;
    final PhotoMapper photoMapper;
    final AlbumMapper albumMapper;
    final MessageMapper messageMapper;

    public HomeServiceIml(UserMapper userMapper, ConnectionMapper connectionMapper, PhotoMapper photoMapper, AlbumMapper albumMapper, MessageMapper messageMapper) {
        this.userMapper = userMapper;
        this.connectionMapper = connectionMapper;
        this.photoMapper = photoMapper;
        this.albumMapper = albumMapper;
        this.messageMapper = messageMapper;
    }
    // 获得在一起起始时间
    @Override
    public List<Connection> getConnections(String uid) {
        QueryWrapper<Connection> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("uid0", uid)
                .or()
                .eq("uid1", uid);
        return connectionMapper.selectList(wrapperUser);
    }
    @Override
    public List<User> getUsers(String uid) {
        QueryWrapper<User> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("uid", uid);
        return userMapper.selectList(wrapperUser);
    }

    @Override
    public void editPerImage(String filename,String uid) {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("uid",uid);
        User user = new User();
        user.setPersonalImage(filename);
        userMapper.update(user,updateWrapper);
    }

    @Override
    public void editPersonName(String name, String pwd,String uid) {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("uid",uid);
        User user = new User();
        user.setName(name);
        user.setPassword(pwd);
        userMapper.update(user,updateWrapper);
    }

    @Override
    public void editConnectionTime(String time, String uid) {
        UpdateWrapper<Connection> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("uid1",uid)
                     .or()
                     .eq("uid0",uid);
        Connection connection = new Connection();
        connection.setStartTime(time);
        connectionMapper.update(connection,updateWrapper);
    }

    @Override
    public void deleteConnect(String uid) {
        QueryWrapper<Connection> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("uid1",uid)
                .or()
                .eq("uid0",uid);
        connectionMapper.delete(wrapperUser);
    }

    @Override
    public void editPersonConnection0(String uid) {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("uid",uid);
        User user = new User();
        user.setIsConnected("0");
        userMapper.update(user,updateWrapper);
    }

    @Override
    public List<Album> selectAlbums(String cid) {
        QueryWrapper<Album> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("cid",cid).orderByAsc("create_time");
        return albumMapper.selectList(wrapperUser);
    }

    @Override
    public List<Message> getMessages(String to) {
        QueryWrapper<Message> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("to_id",to);
        return messageMapper.selectList(wrapperUser);
    }

    @Override
    public List<Album> getAlbums(String cid) {
        QueryWrapper<Album> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("cid",cid);
        return albumMapper.selectList(wrapperUser);
    }

    @Override
    public void deletePhotos(String albumId) {
        QueryWrapper<Photo> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("album_id",albumId);
        photoMapper.delete(wrapperUser);
    }

    @Override
    public void deleteAlbum(String cid) {
        QueryWrapper<Album> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("cid",cid);
        albumMapper.delete(wrapperUser);
    }
}
