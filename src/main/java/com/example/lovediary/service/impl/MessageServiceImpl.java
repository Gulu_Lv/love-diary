package com.example.lovediary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.lovediary.entity.Connection;
import com.example.lovediary.entity.Message;
import com.example.lovediary.entity.User;
import com.example.lovediary.mapper.ConnectionMapper;
import com.example.lovediary.mapper.MessageMapper;
import com.example.lovediary.mapper.UserMapper;
import com.example.lovediary.service.MessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional(rollbackFor = Exception.class)
public class MessageServiceImpl implements MessageService {
    final MessageMapper messageMapper;
    final UserMapper userMapper;
    final ConnectionMapper connectionMapper;

    public MessageServiceImpl(MessageMapper messageMapper, UserMapper userMapper, ConnectionMapper connectionMapper) {
        this.messageMapper = messageMapper;
        this.userMapper = userMapper;
        this.connectionMapper = connectionMapper;
    }

    @Override
    public List<Message> getMessage(String mid) {
        QueryWrapper<Message> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("mid",mid);
        return messageMapper.selectList(wrapperUser);
    }

    @Override
    public List<User> getUser(String uid) {
        QueryWrapper<User> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("uid",uid);
        return userMapper.selectList(wrapperUser);
    }

    @Override
    public void updateUserConnection(String uid) {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("uid",uid);
        User user = new User();
        user.setIsConnected("1");
        userMapper.update(user,updateWrapper);
    }

    @Override
    public void insertConnection(String uid0, String uid1) {
        Date date = new Date();//获取当前的日期
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
        String str = df.format(date);//获取String类型的时间
        Connection connection = new Connection(UUID.randomUUID().toString(),uid0,uid1,str);
        connectionMapper.insert(connection);
    }

    @Override
    public void deleteMessage(String mid) {
        QueryWrapper<Message> query = new QueryWrapper<>();
        query.eq("mid",mid);
        messageMapper.delete(query);
    }
}
