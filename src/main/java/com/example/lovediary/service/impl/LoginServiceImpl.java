package com.example.lovediary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.lovediary.entity.User;
import com.example.lovediary.mapper.UserMapper;
import com.example.lovediary.service.LoginService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional(rollbackFor = Exception.class)
public class LoginServiceImpl implements LoginService {
    final UserMapper userMapper;

    public LoginServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }
    // 登录判断
    @Override
    public List<User> selectUser(String email,String password) {
        QueryWrapper<User> wrapperUser = new QueryWrapper<>();
        wrapperUser.eq("email", email)
                .eq("password", password);
        return userMapper.selectList(wrapperUser);
    }
}

