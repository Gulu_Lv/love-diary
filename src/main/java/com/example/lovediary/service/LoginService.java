package com.example.lovediary.service;


import com.example.lovediary.entity.User;

import java.util.List;

public interface LoginService {
    // 登录判断
    List<User> selectUser(String email,String password);
}
