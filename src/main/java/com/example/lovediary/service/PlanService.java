package com.example.lovediary.service;

import com.example.lovediary.entity.Plan;

import java.util.List;

public interface PlanService {
    List<Plan> selectPlan(String cid);
    List<Plan> getPlan(String pid);
    void editPlan(String pid,String status,String plan);
    void deletePlan(String pid);
    void addPlan(Plan plan);
}
