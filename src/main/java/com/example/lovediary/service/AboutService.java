package com.example.lovediary.service;

import com.example.lovediary.entity.Message;
import com.example.lovediary.entity.User;

import java.util.List;

public interface AboutService {
    // 发出绑定邀请时 被邀请人是否存在
    List<User> selectIsUser(String email);
    // 邀请消息存入数据库
    void insertInvitation(Message message);
}
