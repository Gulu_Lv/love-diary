package com.example.lovediary.service;

import com.example.lovediary.entity.User;

import java.util.List;

public interface RegisterService {
    // 注册判断 是否已经注册
    List<User> selectIsUser(String email);
    // 存入数据库
    void insertUser(User user);
}
