package com.example.lovediary.service;

import com.example.lovediary.entity.Message;
import com.example.lovediary.entity.User;

import java.util.List;

public interface MessageService {
    List<Message> getMessage(String mid);
    List<User> getUser(String uid);
    void updateUserConnection(String uid);
    void insertConnection(String uid0,String uid1);
    void deleteMessage(String mid);
}
