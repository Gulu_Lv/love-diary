package com.example.lovediary.service;

import com.example.lovediary.entity.*;

import java.util.List;

public interface HomeService {
    List<Connection> getConnections(String uid);
    List<User> getUsers(String uid);
    void editPerImage(String filename ,String uid);
    void editPersonName(String name,String pwd,String uid);
    void editConnectionTime(String time,String uid);
    // 用户解绑
    void deleteConnect(String uid);
    // 用户解绑 设 connection 为 0
    void editPersonConnection0(String uid);
    // 查找相册
    List<Album> selectAlbums(String cid);
    List<Message> getMessages(String to);
    List<Album> getAlbums(String cid);
    void deletePhotos(String albumId);
    void deleteAlbum(String cid);
}
