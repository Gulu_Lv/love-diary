package com.example.lovediary.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.lovediary.entity.Connection;
import com.example.lovediary.entity.Plan;
import com.example.lovediary.entity.User;
import com.example.lovediary.service.HomeService;
import com.example.lovediary.service.PlanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "/plan",produces = "text/html; charset=UTF-8")
public class PlanController {
    final PlanService planService;
    final HomeService homeService;

    public PlanController(PlanService planService, HomeService homeService) {
        this.planService = planService;
        this.homeService = homeService;
    }
    @PostMapping(value="/getPlan" ,produces = "application/json;charset=UTF-8")
    public @ResponseBody Plan getPlan(@RequestBody String json){
        String pid = JSONObject.parseObject(json).getString("pid");
        List<Plan> planList = planService.getPlan(pid);
        return planList.get(0);
    }
    @PostMapping("/editPlan")
    public @ResponseBody String editPlan(@RequestBody String json){
        String pid = JSONObject.parseObject(json).getString("pid");
        String plan = JSONObject.parseObject(json).getString("plan");
        String status = JSONObject.parseObject(json).getString("status");
        planService.editPlan(pid, status,plan );
        return "更改成功！";
    }
    @PostMapping("/deletePlan")
    public @ResponseBody String deletePlan(@RequestBody String json){
        String pid = JSONObject.parseObject(json).getString("pid");
        planService.deletePlan(pid);
        return "删除成功！";
    }
    @PostMapping("/addPlan")
    public @ResponseBody String addPlan(@RequestBody String json, HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");
        List<Connection> connectionList = homeService.getConnections(user.getUid());
        Connection connection = connectionList.get(0);
        String plan = JSONObject.parseObject(json).getString("plan");
        LocalDate date = LocalDate.now(); // get the current date 
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Plan plan0 = new Plan(UUID.randomUUID().toString(),connection.getCid(),plan,"0",date.format(formatter));
        planService.addPlan(plan0);
        return "新增成功！";
    }
}
