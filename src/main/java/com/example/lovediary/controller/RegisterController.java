package com.example.lovediary.controller;

import com.example.lovediary.entity.User;
import com.example.lovediary.service.RegisterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "/register",produces = "text/html; charset=UTF-8")
public class RegisterController {
    final RegisterService registerService;
    
    public RegisterController(RegisterService registerService) {
        this.registerService = registerService;
    }
    @GetMapping("/Register")
    public ModelAndView login(ModelAndView model){
        model.setViewName("register");
        return model;
    }
    @PostMapping("/toRegister")
    public ModelAndView checkLogin(ModelAndView model, HttpServletRequest request){
        String email = request.getParameter("email");
        List<User> userList= registerService.selectIsUser(email);
        if (userList !=null && userList.size()!=0){
            model.addObject("errorMsg","该账号已经被注册 请更换");
            model.setViewName("register");
        }else {
            String password = request.getParameter("pwd");
            String name = request.getParameter("name");
            User user = new User(UUID.randomUUID().toString(),name,email,password,"0","1.jpg");
            registerService.insertUser(user);
            model.addObject("Msg","注册成功!请登录");
            model.setViewName("login");
        }
        return model;
    }
}
