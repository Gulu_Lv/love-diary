package com.example.lovediary.controller;

import com.example.lovediary.entity.Album;
import com.example.lovediary.entity.Connection;
import com.example.lovediary.entity.Photo;
import com.example.lovediary.entity.User;
import com.example.lovediary.service.AlbumService;
import com.example.lovediary.service.HomeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "/update",produces = "text/html; charset=UTF-8")
public class UpdataController {
    final HomeService homeService;
    final AlbumService albumService;

    public UpdataController(HomeService homeService, AlbumService albumService) {
        this.homeService = homeService;
        this.albumService = albumService;
    }
    @PostMapping("/personImage")
    public void uploadFile(@RequestParam("file") MultipartFile file,@RequestParam("extension") String extension, HttpServletRequest request) throws IOException {
        User user = (User) request.getSession().getAttribute("user");
        String uid = user.getUid();
        File dir = new File("G:/loveDiary/personalImag/"+uid+extension);
        //Linux路径
//        File dir = new File("/www/javaWeb/loveDiary/personalImag/"+uid+extension);
        if (!dir .getParentFile().exists()) {
            dir .getParentFile().mkdirs();
        }
        if(!dir .exists()) {
            dir .createNewFile();
        }
        file.transferTo(dir);
        homeService.editPerImage(uid+extension,uid);
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        }
    @PostMapping("/Images")
    public void updatePhoto(@RequestParam("file") MultipartFile file,@RequestParam("text") String text,@RequestParam("fileName") String fileName,@RequestParam("albumId") String albumId) throws IOException {
       String[] extension=fileName.split("\\.");
       String pid= UUID.randomUUID().toString();
       File dir = new File("G:/loveDiary/images"+File.separator+pid+"."+extension[1]);
//        File dir = new File("/www/javaWeb/loveDiary/images"+File.separator+pid+"."+extension[1]);
        if (!dir .getParentFile().exists()) {
            dir .getParentFile().mkdirs();
        }
        if(!dir .exists()) {
            dir .createNewFile();
        }
        file.transferTo(dir);
        albumService.addPhoto(new Photo(albumId,pid+"."+extension[1],text,new Date()));
        List<Album> list  = albumService.getAlbum(albumId);
        Album album = list.get(0);
        albumService.addPhotoAlbum(albumId,album.getTotal());
    }
    @PostMapping("/addAlbum")
    public void addAlbum(@RequestParam("file") MultipartFile file,@RequestParam("title") String title,@RequestParam("fileName") String fileName, HttpServletRequest request) throws IOException {
        User user = (User) request.getSession().getAttribute("user");
        List<Connection> connectionList = homeService.getConnections(user.getUid());
        Connection connection = connectionList.get(0);
        String[] extension=fileName.split("\\.");
        String albumId= UUID.randomUUID().toString();
        File dir = new File("G:/loveDiary/albumCover"+ File.separator+albumId+"."+extension[1]);
//        File dir = new File("/www/javaWeb/loveDiary/albumCover"+ File.separator+albumId+"."+extension[1]);
        if (!dir .getParentFile().exists()) {
            dir .getParentFile().mkdirs();
        }
        if(!dir .exists()) {
            dir .createNewFile();
        }
        file.transferTo(dir);
        Album album = new Album(connection.getCid(),albumId,title,albumId+"."+extension[1],0,new Date());
        albumService.addAlbum(album);
    }
}
