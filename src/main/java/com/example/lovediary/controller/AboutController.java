package com.example.lovediary.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.lovediary.entity.Message;
import com.example.lovediary.entity.User;
import com.example.lovediary.service.AboutService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

// about 页面
@Slf4j
@RestController
@RequestMapping(value = "/about")
public class AboutController {
    final AboutService aboutService;

    public AboutController(AboutService aboutService) {
        this.aboutService = aboutService;
    }
    // 没有关联绑定的  发送关联绑定信息
    @PostMapping(value = "/sendInvitation",produces = "text/html;charset=UTF-8")
    public @ResponseBody String sendInvitation(@RequestBody String json){
        String email = JSONObject.parseObject(json).getString("email");
        List<User> userList= aboutService.selectIsUser(email);
        if(userList !=null && userList.size()!=0){
            User user = userList.get(0);
            if (Objects.equals(user.getIsConnected(), "0")){
                String uid0 = JSONObject.parseObject(json).getString("uid");
                System.out.println(uid0);
                String uid1 = user.getUid();
                if (Objects.equals(uid0, uid1)){
                    return "无法和自己建立连接绑定！";
                }else{
                    Message message = new Message(UUID.randomUUID().toString(),uid0, uid1,"0","1","");
                    aboutService.insertInvitation(message);
                    return "发送邀请消息成功！";
                }
            }else {
                return "您输入的邮箱已经和他人绑定！请检查";
            }
        }else {
            return "查无此号！请检查输入";
        }
    }
}
