package com.example.lovediary.controller;


import com.example.lovediary.entity.Connection;
import com.example.lovediary.entity.User;
import com.example.lovediary.service.HomeService;
import com.example.lovediary.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;


@Slf4j
@RestController
@RequestMapping(value = "/login",produces = "text/html; charset=UTF-8")
public class LoginController {
    final LoginService loginService;
    final HomeService homeService;

    public LoginController(LoginService loginService, HomeService homeService) {
        this.loginService = loginService;
        this.homeService = homeService;
    }
    @GetMapping("/Login")
    public ModelAndView login(ModelAndView model){
        model.setViewName("login");
        return model;
    }
    @PostMapping("/toLogin")
    public ModelAndView checkLogin(ModelAndView model,HttpServletRequest request){
        String email = request.getParameter("email");
        String password = request.getParameter("pwd");
        List<User> list= loginService.selectUser(email,password);
        if (list !=null && list.size()!=0){
            User user = list.get(0);
            request.getSession().setAttribute("user", user);
            if(Objects.equals(user.getIsConnected(), "1")){
                tohome(model, user, homeService);
            }else {
                model.setViewName("about");
            }
        }else {
            model.addObject("errorMsg","账号或密码错误");
            model.setViewName("login");
        }
        return model;
    }

    static void tohome(ModelAndView model, User user, HomeService homeService) {
        String uid = user.getUid();
        List<Connection> connectionList = homeService.getConnections(uid);
        Connection connection = connectionList.get(0);
        String uid0 = connection.getUid0();
        String uid1 = connection.getUid1();
        String otherUid;
        if (Objects.equals(uid, uid0)){
            otherUid=uid1;
        }else{
            otherUid = uid0;
        }
        List<User> list0= homeService.getUsers(otherUid);
        User user0 = list0.get(0);
        model.addObject("startTime",connection.getStartTime());
        model.addObject("otherName",user0.getName());
        model.addObject("otherImage",user0.getPersonalImage());
        model.setViewName("home");
    }

}

