package com.example.lovediary.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.lovediary.entity.Message;
import com.example.lovediary.entity.User;
import com.example.lovediary.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/message")
public class MessageController {
    final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping(value = "/binding",produces = "text/html;charset=UTF-8")
    public @ResponseBody String binding(@RequestBody String json){
        String mid = JSONObject.parseObject(json).getString("mid");
        List<Message> list = messageService.getMessage(mid);
        List<User> userList = messageService.getUser(list.get(0).getFromId());
        return userList.get(0).getName()+"想与您绑定,是否接收绑定？";
    }
    @PostMapping(value = "/receivingBinding",produces = "text/html;charset=UTF-8")
    public @ResponseBody void receivingBinding(@RequestBody String json, HttpServletRequest request){
        String mid = JSONObject.parseObject(json).getString("mid");
        List<Message> list = messageService.getMessage(mid);
        Message message =list.get(0);
        messageService.updateUserConnection(message.getFromId());
        messageService.updateUserConnection(message.getToId());
        messageService.insertConnection(message.getFromId(),message.getToId());
        List<User> userList = messageService.getUser(message.getToId());
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", userList.get(0));
        messageService.deleteMessage(mid);
    }
    @PostMapping(value = "/rejectBinding",produces = "text/html;charset=UTF-8")
    public @ResponseBody void rejectBinding(@RequestBody String json){
        String mid = JSONObject.parseObject(json).getString("mid");
        messageService.deleteMessage(mid);
    }
}
