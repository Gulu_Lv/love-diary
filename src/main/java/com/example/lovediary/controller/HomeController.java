package com.example.lovediary.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.lovediary.entity.*;
import com.example.lovediary.service.HomeService;
import com.example.lovediary.service.PlanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping(value = "/home",produces ="text/html; charset=UTF-8" )
public class HomeController {
    final HomeService homeService;
    final PlanService planService;
    public HomeController(HomeService homeService, PlanService planService) {
        this.homeService = homeService;
        this.planService = planService;
    }


    @GetMapping("/Home")
    public ModelAndView home(ModelAndView model,HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");
        LoginController.tohome(model, user, homeService);
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        return model;
    }
    @GetMapping("/About")
    public ModelAndView about(ModelAndView model,HttpServletRequest request){
        model.setViewName("about");
        User user = (User) request.getSession().getAttribute("user");
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        return model;
    }
    @GetMapping("/Album")
    public ModelAndView album(ModelAndView model,HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");
        List<Connection> connectionList = homeService.getConnections(user.getUid());
        Connection connection = connectionList.get(0);
        List<Album> albums = homeService.selectAlbums(connection.getCid());
        model.addObject("albums",albums);
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        model.setViewName("album");
        return model;
    }
    @GetMapping("/Messages")
    public ModelAndView message(ModelAndView model, HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");
        List<Message> messages = homeService.getMessages(user.getUid());
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        model.addObject("messages",messages);
        model.setViewName("message");
        return model;
    }
    @GetMapping("/Person")
    public ModelAndView person(ModelAndView model, HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");
        String uid = user.getUid();
        if (Objects.equals(user.getIsConnected(), "1")){
            List<Connection> connectionList = homeService.getConnections(user.getUid());
            Connection connection = connectionList.get(0);
            String uid0 = connection.getUid0();
            String uid1 = connection.getUid1();
            String otherUid;
            if (Objects.equals(uid, uid0)){
                otherUid=uid1;
            }else{
                otherUid = uid0;
            }
            List<User> list0= homeService.getUsers(otherUid);
            User user0 = list0.get(0);
            model.addObject("otherName",user0.getName());
            model.addObject("startTime",connection.getStartTime());
        }
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        model.setViewName("personal");
        return model;
    }
    @GetMapping("/Plan")
    public ModelAndView getPlan(ModelAndView model, HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");
        List<Connection> connectionList = homeService.getConnections(user.getUid());
        Connection connection = connectionList.get(0);
        List<Plan> planlist = planService.selectPlan(connection.getCid());
        model.addObject("planlist",planlist);
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        model.setViewName("plan");
        return model;
    }
    @PostMapping(value = "/editPerson")
    public @ResponseBody String editPerson(@RequestBody String json, HttpServletRequest request){
        String name = JSONObject.parseObject(json).getString("name");
        String pwd = JSONObject.parseObject(json).getString("pwd");
        User user = (User) request.getSession().getAttribute("user");
        homeService.editPersonName(name, pwd,user.getUid());
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        if (Objects.equals(user.getIsConnected(), "1")){
            String time = JSONObject.parseObject(json).getString("time");
            homeService.editConnectionTime(time, user.getUid());
        }
        return "资料修改完成！";
    }
    @PostMapping(value = "/deleteConnect")
    public @ResponseBody String deleteConnect(HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("user");
        List<Connection> connectionList = homeService.getConnections(user.getUid());
        Connection connection = connectionList.get(0);
        String uid0 = connection.getUid0();
        String uid1 = connection.getUid1();
        String otherUid;
        String cid = connection.getCid();
        List<Album> album_list = homeService.getAlbums(cid);
        if (album_list!=null){
            for (Album album : album_list) {
                String albumId = album.getAlbumId();
                homeService.deletePhotos(albumId);
            }
        }
        homeService.deleteAlbum(cid);
        if (Objects.equals(user.getUid(), uid0)){
            otherUid=uid1;
        }else{
            otherUid = uid0;
        }
        homeService.editPersonConnection0(otherUid);
        homeService.editPersonConnection0(user.getUid());
        List<User> list=homeService.getUsers(user.getUid());
        User newUser = list.get(0);
        request.getSession().removeAttribute("user");
        request.getSession().setAttribute("user", newUser);
        homeService.deleteConnect(user.getUid());
        return "解绑成功！";
    }
}
