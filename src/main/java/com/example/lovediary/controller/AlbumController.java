package com.example.lovediary.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.lovediary.entity.Album;
import com.example.lovediary.entity.Photo;
import com.example.lovediary.service.AlbumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = "/album",produces ="text/html; charset=UTF-8" )
public class AlbumController {
    final AlbumService albumService;

    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }
    @PostMapping(value = "/setAlbumData",produces = "application/json;charset=UTF-8")
    public @ResponseBody Map<Object,Object> setAlbumData(@RequestBody String json){
        String albumId = JSONObject.parseObject(json).getString("albumId");
        String pageNumber = JSONObject.parseObject(json).getString("pageNumber");
        List<Album> list  = albumService.getAlbum(albumId);
        Album album = list.get(0);
        Map<Object,Object> map=albumService.setAlbumData(albumId, Integer.parseInt(pageNumber));
        map.put("totalNumber",album.getTotal());
        map.put("tit",album.getTitle());
        return map;
    }
    @PostMapping(value = "/setEditPhotoText",produces = "application/json;charset=UTF-8")
    public @ResponseBody Photo setEditPhotoText(@RequestBody String json){
        String location = JSONObject.parseObject(json).getString("location");
        List<Photo> list_photos = albumService.setEditPhotoText(location);
        return list_photos.get(0);
    }
    @PostMapping(value = "/editPhotoText",produces = "application/json;charset=UTF-8")
    public @ResponseBody void editPhotoText(@RequestBody String json){
        String location = JSONObject.parseObject(json).getString("location");
        String text = JSONObject.parseObject(json).getString("text");
        albumService.editPhotoText(location, text);
    }
    @PostMapping(value = "/deletePhoto",produces = "application/json;charset=UTF-8")
    public @ResponseBody void deletePhoto(@RequestBody String json){
        String location = JSONObject.parseObject(json).getString("location");
        String albumId = JSONObject.parseObject(json).getString("albumId");
        List<Album> list  = albumService.getAlbum(albumId);
        Album album = list.get(0);
        albumService.deletePhoto(location);
        albumService.deletePhotoAlbum(albumId,album.getTotal());
    }
    @PostMapping(value = "/deleteAlbum",produces = "application/json;charset=UTF-8")
    public @ResponseBody void deleteAlbum(@RequestBody String json){
        String albumId = JSONObject.parseObject(json).getString("albumId");
        albumService.deleteAlbum(albumId);
    }
}
