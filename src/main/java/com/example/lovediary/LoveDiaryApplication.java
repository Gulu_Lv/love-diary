package com.example.lovediary;

import org.springframework.boot.SpringApplication;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com/example/lovediary/mapper")
@SpringBootApplication
public class LoveDiaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoveDiaryApplication.class, args);
	}

}
