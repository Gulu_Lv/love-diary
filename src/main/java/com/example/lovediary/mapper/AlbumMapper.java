package com.example.lovediary.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.lovediary.entity.Album;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AlbumMapper extends BaseMapper<Album> {
}
