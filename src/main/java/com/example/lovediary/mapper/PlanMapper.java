package com.example.lovediary.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.lovediary.entity.Plan;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PlanMapper extends BaseMapper<Plan> {
}
