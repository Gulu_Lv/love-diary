package com.example.lovediary.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.lovediary.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    
}
