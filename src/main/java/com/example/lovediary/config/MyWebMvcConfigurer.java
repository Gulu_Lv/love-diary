package com.example.lovediary.config;

import com.example.lovediary.interceptor.LoggingInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class MyWebMvcConfigurer extends WebMvcConfigurationSupport {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        // 访问绝对路径 访问路径和 存放路径可以自定义，建议存放路径放到配置文件中
        registry.addResourceHandler("/data/personImg/**").addResourceLocations("file:G:/loveDiary/personalImag/");
        registry.addResourceHandler("/update/personImg/**").addResourceLocations("file:G:/loveDiary/images/");
        registry.addResourceHandler("/update/albumCover/**").addResourceLocations("file:G:/loveDiary/albumCover/");

        // linux路径
//        registry.addResourceHandler("/data/personImg/**").addResourceLocations("file:/www/javaWeb/loveDiary/personalImag/");
//        registry.addResourceHandler("/update/personImg/**").addResourceLocations("file:/www/javaWeb/loveDiary/images/");
//        registry.addResourceHandler("/update/albumCover/**").addResourceLocations("file:/www/javaWeb/loveDiary/albumCover/");

        super.addResourceHandlers(registry);
    }
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoggingInterceptor()).addPathPatterns("/**").excludePathPatterns("/static/**","/update/personImg/**","/data/personImg/**","/update/albumCover/**");
        super.addInterceptors(registry);
    }
}
