package com.example.lovediary.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoggingInterceptor implements HandlerInterceptor {
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {   
        // 如果是登陆页面则放行
        if (request.getRequestURI().contains("login") || request.getRequestURI().contains("register")) {
            return true;
        }

        HttpSession session = request.getSession();

        // 如果用户已登陆也放行
        if (session.getAttribute("user") != null) {
            return true;
        }
        // 用户没有登陆跳转到登陆页面
        response.sendRedirect(request.getContextPath() + "/login/Login");
        return false;
    }
}
