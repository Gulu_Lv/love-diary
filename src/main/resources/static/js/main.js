function moreFunction(){
    alert("更多功能正在开发，敬请期待！");
}
// 登录注册时 判断输入框是否为空
function validate() {
    if(document.getElementById('email').value===""){
        alert("邮箱不能为空");
        document.getElementById('email').focus();
        return false;
    }else if(document.getElementById('pwd').value===""){
        alert("密码不能为空");
        document.getElementById('pwd').focus();
        return false;
    }else if(document.getElementById('name').value===""){
        alert("密码不能为空");
        document.getElementById('name').focus();
        return false;
    }
    return true;
}
// about页 发送绑定邀请
function invitation(){
    let email = document.getElementById('about-email').value;
    if (email===""){
        alert("错误！不能未空！");
    }else{
        let uid = document.getElementById('about-uid').value;
        let message="{'email':'"+email+"','uid':'"+uid+"'}";
        $.ajax({
            type : "post",
            url : "/about/sendInvitation",
            contentType:'application/json;charset=utf-8',
            data : message,
            success : function(data) {
                alert(data);
                $('#close-about').click();
            }
        });
    }
}
// home页  时钟每一秒读取前端数据时间
function timeScript(){
    let date = document.getElementById('home-startTime').value;
    let year = date.split('-')[0];
    let mouth = date.split('-')[1]-1;
    let day = date.split('-')[2];
    var together = new Date();
    together.setFullYear(year,mouth, day); 			//时间年月日
    together.setHours(5);						//小时	
    together.setMinutes(20);					//分钟
    together.setSeconds(0);					//秒前一位
    together.setMilliseconds(0);
    timeElapse(together);
}
// home页 定时器任务 
function schedule(){
    timeScript();
    // 每十分钟检查是否更改时间
    setInterval('timeScript()',1000);
}
// home页 时间流逝 时钟效果
function timeElapse(date){
    var current = Date();
    var seconds = (Date.parse(current) - Date.parse(date)) / 1000;
    var days = Math.floor(seconds / (3600 * 24));
    seconds = seconds % (3600 * 24);
    var hours = Math.floor(seconds / 3600);
    if (hours < 10) {
        hours = "0" + hours;
    }
    seconds = seconds % 3600;
    var minutes = Math.floor(seconds / 60);
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    seconds = seconds % 60;
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    var result = "第 <span class=\"digit\">" + days + "</span> 天 <span class=\"digit\">" + hours + "</span> 小时 <span class=\"digit\">" + minutes + "</span> 分钟 <span class=\"digit\">" + seconds + "</span> 秒";
    $("#clock").html(result);
}
// plan页 模态框传值 模态框需要获得pid
function setPidValue(pid){
    let message="{'pid':'"+pid+"'}";
    $.ajax({
        type : "post",
        url : "/plan/getPlan",
        contentType:'application/json;charset=utf-8',
        data : message,
        success : function(data) {
            $('#pid').val(data["pid"]);
            $('#plan').val(data["plan"]);
            $('#status').val(data["status"]);
        }
    });
}
// plan页 编辑计划信息
function editPlan(){
    let pid = document.getElementById('pid').value;
    let plan = document.getElementById('plan').value;
    let status = document.getElementById('status').value;
    if (plan===""){
        alert("错误！计划不能为空!");
    }else{
        let message="{'pid':'"+pid+"','plan':'"+plan+"','status':'"+status+"'}";
        $.ajax({
            type : "post",
            url : "/plan/editPlan",
            contentType:'application/json;charset=utf-8',
            data : message,
            success : function(data) {
               alert(data);
                window.location.href = "/home/Plan";
            }
        });
    }
}
function setPidValue1(pid){
    $('#delete_pid').val(pid);
}
// plan页 删除一条计划
function deletePlan(){
    let pid = document.getElementById('delete_pid').value;
    let message="{'pid':'"+pid+"'}";
    $.ajax({
        type : "post",
        url : "/plan/deletePlan",
        contentType:'application/json;charset=utf-8',
        data : message,
        success : function(data) {
            alert(data);
            window.location.href = "/home/Plan";
        }
    });
}
// plan页 新增一条计划
function addPlan(){
    let plan = document.getElementById('addplan').value;
    let message="{'plan':'"+plan+"'}";
    $.ajax({
        type : "post",
        url : "/plan/addPlan",
        contentType:'application/json;charset=utf-8',
        data : message,
        success : function(data) {
            alert(data);
            window.location.href = "/home/Plan";
        }
    });
}
// about页控制跳转
function controlJump(url){
    let isConnected = document.getElementById('about-isConnected').value;
    if (isConnected==="0"){
        alert("你还未绑定对象，该功能暂未开放！")
    }else{
        window.location.href=url;
    }
}
//personal 页 点击个人头像更换头像 唤醒文件上传input
function imgclick(){
    $("#person_file").click();
}
//personal 页 点击个人头像更换头像 文件上传限制
function image_upload(){
    let file = document.getElementById('person_file');
    let fileName = file.files[0].name;
    let fileSize = file.files[0].size;
    console.log(fileSize);
    console.log(fileName);
    if(fileSize>1024*1024*3){
        alert("文件过大，请选择3M以内的");
        return false;
    }else{
        //返回文件名中最后一个"."的位置，比如文件名为"javascript.html.exe"，则返回的值为15
        const index = fileName.lastIndexOf(".");
        //取出从index开始到最后的字符串，赋值给extension（extension就是该文件的类型）并且转换成小写
        const extension = fileName.substring(index).toLowerCase();
        //这里定义允许用户上传的文件类型
        const allowedType = [".jpg", ".png", ".jpeg"];
        //判断extension是否存在于allowedType中
        let temp ="";
        for(let i=0; i<allowedType.length; i++){
            if(allowedType[i]!==extension){
                temp="false";
            }else {
                temp="true";break;
            }
        }
        if (temp==="true"){
            updateImage(extension);
            return true;
        }else {
            alert("不支持"+extension+"格式");
            return false;
        }
    }
}
// person 页 图片传入后端
function updateImage(extension){
    let file = document.getElementById('person_file').files[0];
    // 单文件上传
    const formData = new FormData();
    //这里需要实例化一个FormData来进行文件上传
    formData.append("file",file);
    formData.append("extension",extension);
    $.ajax({
        type : "post",
        url : "/update/personImage",
        data : formData,
        async: false,
        processData : false,
        contentType : false,
        success : function(){
            alert("头像更改成功！")
            window.location.href="/home/Person";
        }
    });
}
function set2len(str) {
    if (!isNaN(str)) {
        str += '';
    }
    return str.length === 1 ? '0' + str : str;
}
function isDate(str) {
    if (!str) {
        return false;
    }

    var res = str.match(/^(\d{4})(-)(\d{1,2})\2(\d{1,2})$/);
    if (res == null) {
        return false;
    }

    var d = new Date(res[1], Number(res[3])-1, res[4]);

    return d.getFullYear() + '-' + set2len(d.getMonth() + 1) + '-' + set2len(d.getDate());
}
function editPerson(){
    let name = document.getElementById('person-name').value;
    let pwd = document.getElementById('person-pwd').value;
    let isConnected = document.getElementById('about-isConnected').value;
    if (isConnected ==="1"){
        let time = document.getElementById('person-time').value;
        if (name===""){
            alert("错误！名称不能为空！");
            document.getElementById('person-name').focus();
        }else if (pwd===""){
            alert("错误！密码不能为空！");
            document.getElementById('person-pwd').focus();
        }
        else if(time===""){
            alert("错误！时间不能为空！");
            document.getElementById('person-time').focus();
        }
        else if (!isDate(time)){
            alert("错误！时间格式有误！");
            document.getElementById('person-time').focus();
        }else{
            let message="{'name':'"+name+"','time':'"+time+"','pwd':'"+pwd+"'}";
            $.ajax({
                type : "post",
                url : "/home/editPerson",
                contentType:'application/json;charset=utf-8',
                data : message,
                success : function(data) {
                    alert(data);
                    window.location.href = "/home/Person";
                }
            });
        }
    }
    else {
        if (name===""){
            alert("错误！名称不能为空！");
            document.getElementById('person-name').focus();
        }else if (pwd===""){
            alert("错误！密码不能为空！");
            document.getElementById('person-pwd').focus();
        } else{
            let message="{'name':'"+name+"','pwd':'"+pwd+"'}";
            $.ajax({
                type : "post",
                url : "/home/editPerson",
                contentType:'application/json;charset=utf-8',
                data : message,
                success : function(data) {
                    alert(data);
                    window.location.href = "/home/Person";
                }
            });
        }
    }
    
}
function deleteConnect(){
    $.ajax({
        type : "post",
        url : "/home/deleteConnect",
        contentType:'application/json;charset=utf-8',
        success : function(data) {
            alert(data);
            window.location.href = "/home/Person";
        }
    });
}
// 初始化相册里面的内容
function initializationAlbumData(albumId){
    let message="{'albumId':'"+albumId+"','pageNumber':'1'}";
    $.ajax({
        type : "post",
        url : "/album/setAlbumData",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function(data) {
            $('#album-title').html(data.tit);
            $('#albumPage').val("1");
            $('#albumId').val(albumId);
            $('#totalPage').val(data.totalPageNumber);
            showPicture(data.photos,data.totalPageNumber,data.totalNumber,1)
        }
    });
}
function formatDate(value) {
    var date = new Date(value);
    var y = date.getFullYear(),
        m = date.getMonth() + 1,
        d = date.getDate()
    if (m < 10) { m = '0' + m; }
    if (d < 10) { d = '0' + d; }
    return y + '-' + m + '-' + d;
}
function showPicture(data,totalPages,total,currentPage) {
    var a=[];
    for (let i=0;i<data.length;i++){
        var str="<div class=\"albumBox\">" +
            "<div class=\"photoBox\">" +
            "<img class=\"photo\" src=\"/update/personImg/"+data[i].location+"\" alt=\"\">" +
            "</div><div class=\"albumText\">" +
            "<label>描述文本:</label><span style=\"color:black;\">"+data[i].text+"</span><br>" +
            "<label>上传时间:</label><span style=\"color:black;\">"+formatDate(data[i].createTime)+"</span><br>" +
            "<button href=\"#photo-model\" data-toggle=\"modal\" onclick=\"setEditPhotoText('"+data[i].location+"')\">编辑</button></div></div>";
        a.push(str);
    }
    var str1 =a.join("");
    a=null;
    $("#albumBoxs").html(str1);
    var str0="<span>共"+total+"张，共"+totalPages+"页，当前在第"+currentPage+"页</span> " +
        "<button type=\"button\" id=\"btn_return\" class=\"btn btn-primary\" onclick=\"previousPage()\" >上一页</button>" +
        "<button type=\"button\" id=\"btn_next\" class=\"btn btn-primary deleteAlbum\" onclick=\"nextPage()\" >下一页</button>"
    $("#albumText0").html(str0);
}
// 上一页
function previousPage(){
    let currentPage = document.getElementById('albumPage').value;
    if (currentPage==="1"){
        alert("已经到顶了！");
    }else {
        setAlbumData(currentPage-1);
    }
}
// 下一页
function nextPage(){
    let currentPage = document.getElementById('albumPage').value;
    let totalPages = document.getElementById('totalPage').value;
    if (currentPage===totalPages){
        alert("已经到底了！");
    }else {
        setAlbumData(Number(currentPage)+1);
    }
}
function setAlbumData(pageNumber){
    let albumId = document.getElementById('albumId').value;
    let message="{'albumId':'"+albumId+"','pageNumber':'"+pageNumber+"'}";
    $.ajax({
        type : "post",
        url : "/album/setAlbumData",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function(data) {
            $('#album-title').html(data.tit);
            $('#albumPage').val(pageNumber);
            $('#albumId').val(albumId);
            $('#totalPage').val(data.totalPageNumber);
            showPicture(data.photos,data.totalPageNumber,data.totalNumber,pageNumber)
        }
    });
}
function setEditPhotoText(location){
    let message="{'location':'"+location+"'}";
    $.ajax({
        type : "post",
        url : "/album/setEditPhotoText",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function(data) {
            showEditPhotoText(data)
            $('#photo-location').val(data["location"]);
        }
    });
}
function showEditPhotoText(data){
    var str="<img style=\"width:100%;height:auto\" src=\"/update/personImg/"+data["location"]+"\" alt=\"\">\n" +
        "                <div class=\"form-group\">\n" +
        "                    <label for=\"photo-text\">描述文本</label>\n" +
        "                    <input type=\"text\" name=\"photo-text\" class=\"form-control\" id=\"photo-text\" value=\""+data["text"]+"\"  placeholder=\"10个字以内最好\">\n" +
        "                </div>";
    $("#edit-photo-body").html(str);
}
function editPhotoText(){
    let location = document.getElementById('photo-location').value;
    let currentPage = document.getElementById('albumPage').value;
    let text = document.getElementById('photo-text').value;
    let message="{'location':'"+location+"','text':'"+text+"'}";
    $.ajax({
        type : "post",
        url : "/album/editPhotoText",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function() {
            $('#photo-location').val(location);
            $('#photo-text').val(text);
            setAlbumData(currentPage);
            alert("修改成功！");
        }
    });
}
function deletePhoto(){
    let location = document.getElementById('photo-location').value;
    let currentPage = document.getElementById('albumPage').value;
    let albumId = document.getElementById('albumId').value;
    let message="{'location':'"+location+"','albumId':'"+albumId+"'}";
    $.ajax({
        type : "post",
        url : "/album/deletePhoto",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function() {
            setAlbumData(currentPage);
            alert("删除成功！");
            $("#close").click();
        }
    });
}
function downloadPhoto(){
    let location = document.getElementById('photo-location').value;
    window.open("/update/personImg/"+location);
    let a = document.createElement('a')
// 定义下载名称
    a.download = location
// 隐藏标签
    a.style.display = 'none'
// 设置文件路径
    a.href = "/update/personImg/"+location;
// 将创建的标签插入dom
    document.body.appendChild(a)
// 点击标签，执行下载
    a.click()
// 将标签从dom移除
    document.body.removeChild(a)
}
function deleteAlbum(){
    let albumId = document.getElementById('albumId').value;
    let message="{'albumId':'"+albumId+"'}";
    $.ajax({
        type : "post",
        url : "/album/deleteAlbum",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function() {
            alert("删除成功！");
            window.location.href ="/home/Album";
        }
    });
}
function checkUpdatePhoto(){
    let file = document.getElementById('person_file');
    let fileName = file.files[0].name;
    let fileSize = file.files[0].size;
    console.log(fileSize);
    console.log(fileName);
    if(fileSize>1024*1024*5){
        alert("文件过大，请选择5M以内的");
        return false;
    }else{
        //返回文件名中最后一个"."的位置，比如文件名为"javascript.html.exe"，则返回的值为15
        const index = fileName.lastIndexOf(".");
        //取出从index开始到最后的字符串，赋值给extension（extension就是该文件的类型）并且转换成小写
        const extension = fileName.substring(index).toLowerCase();
        //这里定义允许用户上传的文件类型
        const allowedType = [".jpg", ".png", ".jpeg"];
        //判断extension是否存在于allowedType中
        let temp ="";
        for(let i=0; i<allowedType.length; i++){
            if(allowedType[i]!==extension){
                temp="false";
            }else {
                temp="true";break;
            }
        }
        if (temp==="true"){
            $("#filename").html(fileName);
            return true;
        }else {
            alert("不支持"+extension+"格式");
            return false;
        }
    }
}
function updatePhoto() {
    let text = document.getElementById('updatePhotoText').value;
    let fileName = document.getElementById('filename').innerHTML;
    if (fileName===""){
        alert("错误，未选择文件!");
    }else if (text===""){
        alert("错误，描述文本为空");
        document.getElementById('updatePhotoText').focus();
    }else {
        let file = document.getElementById('person_file').files[0];
        let albumId =document.getElementById('albumId').value;
        let currentPage = document.getElementById('albumPage').value;
        // 单文件上传
        const formData = new FormData();
        //这里需要实例化一个FormData来进行文件上传
        formData.append("file",file);
        formData.append("fileName",fileName);
        formData.append("text",text);
        formData.append("albumId",albumId);
        $.ajax({
            type : "post",
            url : "/update/Images",
            data : formData,
            async: false,
            processData : false,
            contentType : false,
            success : function(){
                alert("图片上传成功！")
                $("#close0").click();
                setAlbumData(currentPage);
            }
        });
    }

}
function newAlbumclick(){
    $("#person_file-newAlbum").click();
}
function checkaddAlbum(){
    let file = document.getElementById('person_file-newAlbum');
    let fileName = file.files[0].name;
    let fileSize = file.files[0].size;
    console.log(fileSize);
    console.log(fileName);
    if(fileSize>1024*1024*5){
        alert("文件过大，请选择5M以内的");
        return false;
    }else{
        //返回文件名中最后一个"."的位置，比如文件名为"javascript.html.exe"，则返回的值为15
        const index = fileName.lastIndexOf(".");
        //取出从index开始到最后的字符串，赋值给extension（extension就是该文件的类型）并且转换成小写
        const extension = fileName.substring(index).toLowerCase();
        //这里定义允许用户上传的文件类型
        const allowedType = [".jpg", ".png", ".jpeg"];
        //判断extension是否存在于allowedType中
        let temp ="";
        for(let i=0; i<allowedType.length; i++){
            if(allowedType[i]!==extension){
                temp="false";
            }else {
                temp="true";break;
            }
        }
        if (temp==="true"){
            $("#newAlbumFilename").html(fileName);
            return true;
        }else {
            alert("不支持"+extension+"格式");
            return false;
        }
    }
}
function addAlbum(){
    let title = document.getElementById('newAlbumTitle').value;
    let fileName = document.getElementById('newAlbumFilename').innerHTML;
    if (fileName===""){
        alert("错误，未选择文件!");
    }else if (title===""){
        alert("错误，描述文本为空");
        document.getElementById('updatePhotoText').focus();
    }else {
        let file = document.getElementById('person_file-newAlbum').files[0];
        // 单文件上传
        const formData = new FormData();
        //这里需要实例化一个FormData来进行文件上传
        formData.append("file",file);
        formData.append("fileName",fileName);
        formData.append("title",title);
        $.ajax({
            type : "post",
            url : "/update/addAlbum",
            data : formData,
            async: false,
            processData : false,
            contentType : false,
            success : function(){
                alert("相册创建成功！")
                $("#close-myAddNewAlbum").click();
                window.location.href = "/home/Album";
            }
        });
    }
}
function binding(mid){
    let message="{'mid':'"+mid+"'}";
    $.ajax({
        type : "post",
        url : "/message/binding",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function(data) {
            $("#mid").val(mid);
            $("#BindingText").html(data);
        }
    });
}
function receivingBinding(){
    let mid = document.getElementById('mid').value;
    let message="{'mid':'"+mid+"'}";
    $.ajax({
        type : "post",
        url : "/message/receivingBinding",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function() {
            alert("绑定成功！快通知你的宝贝吧！");
            window.location.href = "/home/Messages";
        }
    });
}
function rejectBinding(){
    let mid = document.getElementById('mid').value;
    let message="{'mid':'"+mid+"'}";
    $.ajax({
        type : "post",
        url : "/message/rejectBinding",
        contentType:'application/json;charset=utf-8',
        data:message,
        success : function() {
            alert("拒绝成功！");
            window.location.href = "/home/Messages";
        }
    });
}